package com.middaydreamz.shobdokolpo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity {
    SharedPreferences sobdokolpoPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initializeSharedPreference();
    }

    public void play(View view) {
        ////
        Intent i = new Intent(this, LevelSelectorActivity.class);
        startActivity(i);
        //overridePendingTransition(R.anim.slide_enter_top_bottom,R.anim.slide_exit_top_bottom);
        overridePendingTransition(R.anim.slide_enter_top_bottom,R.anim.slide_exit_top_bottom);
        finish();

    }

    public void moreGames(View view) {
        Toast.makeText(this, "More Games", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_enter_bottom_up,R.anim.slide_exit_bottom_up);
    }

    public void contact(View v) {
        View view = getLayoutInflater().inflate(R.layout.contact, null, false);

        Button dismiss = (Button) view.findViewById(R.id.dismiss);
        TextView website = (TextView) view.findViewById(R.id.website);

        final AlertDialog.Builder a = new AlertDialog.Builder(this).setView(view).setCancelable(true);

        final AlertDialog b = a.create();
        b.show();
        // b.getWindow().setLayout(600, 800);
        b.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                b.dismiss();
            }
        });

        String t = "Official Site: " + "<a href=\"" + "http://middaydreamz.com" + "\">" + "middaydreamz.com" + "</a>";

        Spanned s;
        if (Build.VERSION.SDK_INT >= 24) {
            s = Html.fromHtml(t, Html.FROM_HTML_MODE_LEGACY);
        } else {
            s = Html.fromHtml(t);
        }

        website.setText(s);
        website.setClickable(true);
        website.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void exit(View view) {
        onBackPressed();
    }
    void initializeSharedPreference(){
        sobdokolpoPreference=getSharedPreferences(getString(R.string.sobdokolpoPreference),MODE_PRIVATE);
        //int levelPreSchool=sobdokolpoPreference.getInt(getString(R.string.levelPreSchool),0);
        int levelPrimary=sobdokolpoPreference.getInt(getString(R.string.levelPrimary),0);
        int levelJunior=sobdokolpoPreference.getInt(getString(R.string.levelJunior),0);
        int levelSecondary=sobdokolpoPreference.getInt(getString(R.string.levelSecondary),0);
        int levelAdvance=sobdokolpoPreference.getInt(getString(R.string.levelAdvance),0);
        SharedPreferences.Editor editor=sobdokolpoPreference.edit();
        //editor.putInt(getString(R.string.levelPreSchool),levelPreSchool);
        editor.putInt(getString(R.string.levelPrimary),levelPrimary);
        editor.putInt(getString(R.string.levelJunior),levelJunior);
        editor.putInt(getString(R.string.levelSecondary),levelSecondary);
        editor.putInt(getString(R.string.levelAdvance),levelAdvance);
        editor.commit();




    }
}
