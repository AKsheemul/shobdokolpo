package com.middaydreamz.shobdokolpo;

import android.content.Context;
import android.util.Log;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

/**
 * Created by FAISAL on 6/10/2017.
 */

public class Utility {
    private Context context;
    String levelPreSchool;
    String levelPrimary;
    String levelJunior;
    String levelSecondary;
    String levelAdvance;

    public static HashMap<String,String> modelFileName;
    public static HashMap<String,String>solutionFileName;

    public Utility(Context context) {
        this.context = context;
    }

    public static HashMap<String,List<String[]>> model;
    public static HashMap<String,List<String[]>>solution;



    public static List<int[]> generateFlag(List<String[]> solution, List<String[]> model) {
        List<int[]> flag = new ArrayList<>();
        Log.d("size of solutionData ",""+solution.size());
        for (int i = 0; i < solution.size(); i++) {
            String[] solutionIth = solution.get(i);
            String[] modelIth = model.get(i);
            int[] flagIth = new int[42];

            for (int j = 0; j < solutionIth.length; j++) {
                if (solutionIth[j].equals(modelIth[j]) && !solutionIth[j].equals("#") ) flagIth[j] = 1;
                else if (solutionIth[j].equals(modelIth[j]) && solutionIth[j].equals("#"))
                    flagIth[j] =3;
                else if (solutionIth[j].equals("#")) flagIth[j] = 2;
                else flagIth[j] = 0;

                Log.d("flag"+i," "+j+" "+flagIth[j]);
            }
            //Log.d("Flag: ",""+ ++i+""+flag);
            flag.add(flagIth);
        }
        return flag;
    }

    public static List<String[]> readFile(InputStream inputStream) {
        //Log.d("Inside","InsideFileRead");
        List<String[]> list = new ArrayList<>();
        //AssetManager am = getApplicationContext().getAssets();

            //InputStream is = am.open(fileName);
            Scanner scanner = new Scanner(inputStream);
            int count=0;
            while (scanner.hasNext()) {
                String line = scanner.nextLine();
                Log.d("Line",line);
                String[] data = line.split(",");
                Log.d("Data String Length", "size "+"" + ++count+"    " +data.length);
                for (int i = 0; i < data.length; i++) {
                    String str = data[i].trim();
                   /* if (str.equals("#")) {
                        str = "";
                    }*/
                    data[i] = str;
                }
                list.add(data);
                Log.d("Tag", line);
            }
            scanner.close();
        return list;

    }
    static {
        model=new HashMap<>();
        solution=new HashMap<>();
    }
}
