package com.middaydreamz.shobdokolpo;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by USER on 5/28/2017.
 */

public class CustomLinearLayout extends LinearLayout {
    private int position;
    private int flag;
    private int colorContent;
    private int colorBackground;
    private Drawable drawableBackground;

    public Drawable getDrawableBackground() {
        return drawableBackground;
    }

    public void setDrawableBackground(Drawable drawableBackground) {
        this.drawableBackground = drawableBackground;
    }

    public int getColorContent() {
        return colorContent;
    }

    public void setColorContent(int colorContent) {
        this.colorContent = colorContent;
    }

    public int getColorBackground() {
        return colorBackground;
    }

    public void setColorBackground(int colorBackground) {
        this.colorBackground = colorBackground;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }


    public CustomLinearLayout(Context context) {
        super(context);
    }

    public CustomLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

   /* public CustomLinearLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }*/
}
