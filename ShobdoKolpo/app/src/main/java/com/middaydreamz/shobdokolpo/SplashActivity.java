package com.middaydreamz.shobdokolpo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

public class SplashActivity extends AppCompatActivity implements
        OnStartDragListener {
    TextView toolbarLevel;
    ImageButton backButton;
    ImageButton refreshButton;
    int currentLevelBest;

    RecyclerView rv;
    RVAdapter rva;
    final int noOfCol = 6;
    ItemTouchHelper touchHelper;
    public static Drawable[] viewBackgroundDrawable;
    public static int[]viewContentColor;
    public static int[] colorBackground = new int[42];
    public static Drawable[] drawableBackground=new Drawable[42];
    public static int[] colorContent = new int[42];

    List<DataContainer> list = new ArrayList<>();
    List<String[]> solutionList;
    List<int[]> flagList;
    public List<String[]> dataList;
    final int colorWhite = Color.parseColor("#ffffff");
    final int colorBlue = Color.parseColor("#01234b");
    final int colorSkyBlue = Color.parseColor("#87ceeb");
    static Drawable drawableFixed;
    static Drawable drawableBlank;
    static Drawable drawableDragable;
    static Drawable drawableFillable;
    static Drawable drawableSolution;
    static Drawable drawableWrong;
    String gameLevelName;
    int levelNumber;
    SharedPreferences sp;
    Intent currentIntent;


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i=new Intent(this,LevelIndicatorActivity.class);
        i.putExtra(getString(R.string.gameLevelName),gameLevelName);
        startActivity(i);
        overridePendingTransition(R.anim.slide_enter_bottom_up,R.anim.slide_exit_bottom_up);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getIntentData();
        initializeView();




        /*String[] data = {"N", "", "M", "","", "", "A", "A","",
                "E", "", "", "", "L", "E", ""};
        String []solution={"N", "A", "M", "E","", "", "A", "","",
                "", "L", "", "", "", "E", ""};
        int []flag={1,0,1,0,-1,-1,1,2,-1,2,0,-1,-1,2,1,-1};*/
        /*String[] data={"","আ","","মা","ন","",
                "ক","","মা","","","",
                "সু","বি","","","ন","",
                "","","ন","","","স",
                "তু","ধা","","ম","","",
                "ন","","মা","","সি","",
                "স","","","ন","","লা"};
        String[] solution={"","আ","স","মা","ন","",
                "","","মা","","তু","",
                "সু","বি","ধা","","ন","",
                "","","ন","","","",
                "","","","ম","","","",
                "","মা","ন","সি","ক","","","","ন","","লা"};
        int []flag={-1,1,0,1,1,-1,
                     2,-1,1,-1,0,-1,
                     1,1,0,-1,1,-1,
                    -1,-1,1,-1,-1,2,
                     2,2,-1,1,-1,-1,
                     2,-1,1,0,1,0,
                    2,-1,-1,1,-1,1
        };*/
       /* dataList = readFile("model.txt");
        String[] data = dataList.get(level);
        solutionList = readFile("solution.txt");
        String[] solution = solutionList.get(level);*/
        dataList=Utility.model.get(gameLevelName);
        String[] data=dataList.get(levelNumber);
        solutionList=Utility.solution.get(gameLevelName);
        String[] solution=solutionList.get(levelNumber);
        flagList = generateFlag(solutionList, dataList);
        //int[] flag = flagList.get(0);
        //int[] flag = flagList.get(level);
        int []flag=flagList.get(levelNumber);
        String[] tempString = dataList.get(0);
        for (int i = 0; i < tempString.length; i++) {
            Log.d("Value", "value " + tempString[i].trim().equals(""));
        }
        Log.d("FileSize", "Model size " + dataList.get(0).length);
        Log.d("FileSize", "Solution size " + solutionList.get(0).length);

        drawableFixed= ContextCompat.getDrawable(getApplicationContext(),R.drawable.grid_fixed);
        drawableBlank=ContextCompat.getDrawable(getApplicationContext(),R.drawable.grid_blank);
        drawableDragable=ContextCompat.getDrawable(getApplicationContext(),R.drawable.grid_dragable);
        drawableFillable=ContextCompat.getDrawable(getApplicationContext(),R.drawable.grid_fillable);
        drawableSolution=ContextCompat.getDrawable(getApplicationContext(),R.drawable.grid_solution);
        drawableWrong=ContextCompat.getDrawable(getApplicationContext(),R.drawable.grid_wrong);
        viewBackgroundDrawable=new Drawable[4];
        viewBackgroundDrawable[0]=drawableFillable;
        viewBackgroundDrawable[1]=drawableFixed;
        viewBackgroundDrawable[2]=drawableDragable;
        viewBackgroundDrawable[3]=drawableBlank;
        viewContentColor=new int[4];
        viewContentColor[0]=colorWhite;
        viewContentColor[1]=colorWhite;
        viewContentColor[2]=colorBlue;
        viewContentColor[3]=colorBlue;


        /*String[] data = {"", "N", "T", "","", "O", "", "D","",
                "", "A", "", "", "E", "", ""};
        String []solution={"A", "N", "T", "","", "O", "", "","",
                "D", "", "", "", "E", "", ""};*/

        for (int i = 0; i < flag.length; i++) {
            if (flag[i] == 0) {
                colorBackground[i] = colorSkyBlue;
                drawableBackground[i]=drawableFillable;
                colorContent[i] = colorWhite;
            } else if (flag[i] == 1) {
                drawableBackground[i]=drawableFixed;
                colorBackground[i] = colorBlue;
                colorContent[i] = colorWhite;
            } else if(flag[i]==3){
                drawableBackground[i]=drawableBlank;
                colorBackground[i] = colorWhite;
                colorContent[i] = colorBlue;
            }
            else{
                drawableBackground[i]=drawableDragable;
                colorContent[i]=colorBlue;
            }
        }
        //int []flag={0,1,1,-1,-1,1,-1,2,-1,0,2,-1,-1,1,-1,-1};
        //int []colorBackground={colorSkyBlue,colorBlue,colorBlue,colorWhite,colorWhite,colorBlue,colorWhite,colorWhite,colorWhite,colorSkyBlue,colorWhite,colorWhite,colorWhite,colorBlue,colorWhite,colorWhite};
        //int []colorContent={colorWhite,colorWhite,colorWhite,colorBlue,colorBlue,colorWhite,colorBlue,colorBlue,colorBlue,colorWhite,colorBlue,colorBlue,colorBlue,colorWhite,colorBlue,colorBlue};
        for (int i = 0; i < data.length; i++) {
            DataContainer temp = new DataContainer(data[i], solution[i], flag[i], drawableBackground[i], colorContent[i]);
            list.add(temp);
        }
        rv = (RecyclerView) findViewById(R.id.gameGrid);
        rv.setLayoutManager(new GridLayoutManager(this, noOfCol));
        rva = new RVAdapter(list, this);
        rv.setAdapter(rva);

    }

    void showNext(){


    }

    public List<String[]> readFile(String fileName) {
        Log.d("Inside","InsideFileRead");
        List<String[]> list = new ArrayList<>();
        AssetManager am = getApplicationContext().getAssets();
        try {
            InputStream is = am.open(fileName);
            Scanner scanner = new Scanner(is);
            int count=0;
            while (scanner.hasNext()) {
                String line = scanner.nextLine();
                Log.d("Line",line);
                String[] data = line.split(",");
                Log.d("Data String Length", "size "+"" + ++count+"    " +data.length);
                for (int i = 0; i < data.length; i++) {
                    String str = data[i].trim();
                   /* if (str.equals("#")) {
                        str = "";
                    }*/
                    data[i] = str;
                }
                list.add(data);
                Log.d("Tag", line);
            }
            scanner.close();
            //FileReader fileReader=new FileReader(is);
        } catch (IOException e) {
            Log.d("Tag", "File not found");

            e.printStackTrace();
        }
        return list;

    }

    private List<String[]> setData(String fileName) {
        List<String[]> list = new ArrayList<>();
        String str;
        try {
            FileReader fileReader = new FileReader(fileName);
            BufferedReader bfr = new BufferedReader(fileReader);
            while ((str = bfr.readLine()) != null) {
                String[] data = this.split(str);
                list.add(data);
            }
            bfr.close();
        } catch (IOException e) {

        }
        return list;
    }

    private String[] split(String str) {
        String[] data = new String[42];
        String temp;
        int i = 0;
        StringTokenizer st = new StringTokenizer(str, ",");
        while (st.hasMoreElements()) {

            temp = (String) st.nextElement();
            temp.trim();
            data[i++] = temp;
        }
        return data;
    }

    private List<int[]> generateFlag(List<String[]> solution, List<String[]> model) {
        List<int[]> flag = new ArrayList<>();
        Log.d("size of solutionData ",""+solution.size());
        for (int i = 0; i < solution.size(); i++) {
            String[] solutionIth = solution.get(i);
            String[] modelIth = model.get(i);
            int[] flagIth = new int[42];

            for (int j = 0; j < solutionIth.length; j++) {
                if (solutionIth[j].equals(modelIth[j]) && !solutionIth[j].equals("#") ) flagIth[j] = 1;
                else if (solutionIth[j].equals(modelIth[j]) && solutionIth[j].equals("#"))
                    flagIth[j] =3;
                else if (solutionIth[j].equals("#")) flagIth[j] = 2;
                else flagIth[j] = 0;

                Log.d("flag"+i," "+j+" "+flagIth[j]);
            }
            //Log.d("Flag: ",""+ ++i+""+flag);
            flag.add(flagIth);
        }
        return flag;
    }


    // Extend the Callback class
    ItemTouchHelper.Callback _ithCallback = new ItemTouchHelper.Callback() {
        //and in your imlpementaion of
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            // get the viewHolder's and target's positions in your adapter data, swap them

            Toast.makeText(SplashActivity.this, "Moving Item " + viewHolder.getAdapterPosition() + " " + target.getAdapterPosition(), Toast.LENGTH_SHORT).show();
            Collections.swap(list, viewHolder.getAdapterPosition(), target.getAdapterPosition());
            // and notify the adapter that its dataset has changed
            rva.notifyItemMoved(viewHolder.getAdapterPosition(), target.getAdapterPosition());
            return true;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            //TODO
            Toast.makeText(SplashActivity.this, "Sawping Item " + viewHolder.getAdapterPosition(), Toast.LENGTH_SHORT).show();


        }

        //defines the enabled move directions in each state (idle, swiping, dragging).
        @Override
        public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return makeFlag(ItemTouchHelper.ACTION_STATE_DRAG,
                    ItemTouchHelper.DOWN | ItemTouchHelper.UP | ItemTouchHelper.START | ItemTouchHelper.END);
        }


        @Override
        public boolean isLongPressDragEnabled() {
            return true;
        }

        @Override
        public boolean isItemViewSwipeEnabled() {
            return true;
        }
    };

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        touchHelper.startDrag(viewHolder);
    }
    void getIntentData(){
        Intent intent=getIntent();
        levelNumber=intent.getIntExtra("level",1);
        gameLevelName=intent.getStringExtra(getString(R.string.gameLevelName));
    }
    void initializeView(){
        toolbarLevel=(TextView)findViewById(R.id.level);
        refreshButton=(ImageButton)findViewById(R.id.refresh);
        toolbarLevel.setText(gameLevelName);
        refreshButton.setOnClickListener(null);
        backButton=(ImageButton)findViewById(R.id.back);
        backButton.setOnClickListener(SplachActivityListener);
        refreshButton.setOnClickListener(SplachActivityListener);
    }

    View.OnClickListener SplachActivityListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.back:
                    Log.d("BackPress","BackPressedFromSplash");
                    onBackPressed();
                    break;
                case R.id.refresh:
                    refreshCurrentLevel();
                    break;
            }
        }
    };
    void refreshCurrentLevel(){
        sp=getSharedPreferences(getString(R.string.sobdokolpoPreference), Context.MODE_PRIVATE);
        currentIntent=getIntent();
       /* currentLevel=currentIntent.getIntExtra("level",1);
        currentGameLevelName=currentIntent.getStringExtra(getString(R.string.gameLevelName));*/
        currentLevelBest=sp.getInt(gameLevelName,1);
        Intent intent=new Intent(this, SplashActivity.class);
        intent.putExtra("level",levelNumber);
        intent.putExtra(getString(R.string.gameLevelName),gameLevelName);
        startActivity(intent);
        //overridePendingTransition(R.anim.slide_enter_top_bottom,R.anim.slide_exit_top_bottom);
        finish();
    }
}
