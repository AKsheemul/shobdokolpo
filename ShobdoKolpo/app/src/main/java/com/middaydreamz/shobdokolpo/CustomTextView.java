package com.middaydreamz.shobdokolpo;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by USER on 5/26/2017.
 */

public class CustomTextView extends AppCompatTextView {
    private int position;
    private int flag;

    public int getFlag() {
        return flag;
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public CustomTextView(Context context) {
        super(context);

    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getPosition() {

        return position;
    }
}
