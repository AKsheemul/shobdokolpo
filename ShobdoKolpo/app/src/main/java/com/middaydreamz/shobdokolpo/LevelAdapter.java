package com.middaydreamz.shobdokolpo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;

/**
 * Created by USER on 6/1/2017.
 */

public class LevelAdapter extends RecyclerView.Adapter<LevelAdapter.LevelViewHolder> {
    ArrayList<Integer> list_temp;
    Activity activity;
    SharedPreferences preferences;
    String gameLevelName;
    int gameLevelScore;

    public LevelAdapter(ArrayList<Integer> list, Activity activity) {
        this.list_temp = list;
        this.activity = activity;
        getData();


    }

    @Override
    public LevelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_level_indicator, parent, false);

        return new LevelViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final LevelViewHolder holder, final int position) {

        // Log.d("TagPosition","position :"+position+", level :"+tempLevel);
        int pos = position + 1;
        String temp = "" + pos;
        holder.levelIndicator.setText(temp);
        holder.levelIndicator.setPosition(position);
        Log.d("gameLevel", position + "   " + gameLevelScore);
        if (position < gameLevelScore) {
            Drawable drawable = ContextCompat.getDrawable(activity.getApplicationContext(), R.drawable.border_after_selected);
            holder.levelIndicator.setBackgroundDrawable(drawable);
            //holder.levelIndicator.setBackgroundColor(Color.parseColor("#00ff00"));
        } else if (position == gameLevelScore) {
            Drawable drawable = ContextCompat.getDrawable(activity.getApplicationContext(), R.drawable.border_current_selection);
            holder.levelIndicator.setBackgroundDrawable(drawable);
        } else {
            Drawable drawable = ContextCompat.getDrawable(activity.getApplicationContext(), R.drawable.border_before_selected);
            holder.levelIndicator.setBackgroundDrawable(drawable);
            // holder.levelIndicator.setBackground(ContextCompat.getResources().getDrawable(activity.getApplicationContext(),R.drawable.border));
            //holder.levelIndicator.setBackgroundColor(Color.parseColor("#01234b"));
        }
        if (position <= gameLevelScore) {
            holder.levelIndicator.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CustomTextView custom = (CustomTextView) view;
                    int level = custom.getPosition();
                    Intent intent = new Intent(activity.getBaseContext(), SplashActivity.class);
                    intent.putExtra("level", level);
                    //intent.putExtra(activity.getString(R.string.gameLevelScore),gameLevelScore);
                    intent.putExtra(activity.getString(R.string.gameLevelName), gameLevelName);
                    activity.startActivity(intent);
                    activity.overridePendingTransition(R.anim.slide_enter_top_bottom, R.anim.slide_exit_top_bottom);
                    activity.finish();
                }
            });
        } else {
            holder.levelIndicator.setOnClickListener(null);
        }

        Log.d("Position", "Position " + position);

    }

    @Override
    public int getItemCount() {
        return list_temp.size();
    }

    public class LevelViewHolder extends RecyclerView.ViewHolder {
        CustomTextView levelIndicator;
        LinearLayout levelLayout;

        public LevelViewHolder(View itemView) {
            super(itemView);
            levelLayout = (LinearLayout) itemView.findViewById(R.id.faisalLayout);
            levelIndicator = (CustomTextView) itemView.findViewById(R.id.faisal);
        }
    }

    void getData() {
        preferences = activity.getSharedPreferences(activity.getString(R.string.sobdokolpoPreference), Context.MODE_PRIVATE);
        Intent intent = activity.getIntent();
        gameLevelName = intent.getStringExtra(activity.getString(R.string.gameLevelName));
        gameLevelScore = preferences.getInt(gameLevelName, 0);
    }
}
