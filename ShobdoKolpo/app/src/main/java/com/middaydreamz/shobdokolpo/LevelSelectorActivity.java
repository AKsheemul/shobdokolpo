package com.middaydreamz.shobdokolpo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class LevelSelectorActivity extends AppCompatActivity {

    ImageButton back, refresh;
    TextView level;
    SharedPreferences sobdokolpoPreference;
    TextView levelOne;
    TextView levelTwo;
    TextView levelThree;
    TextView levelFour;
    TextView levelFive;
    int levelPreSchool;
    int levelPrimary;
    int levelJunior;
    int levelSecondary;
    int levelAdvance;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(this, HomeActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_enter_bottom_up,R.anim.slide_exit_bottom_up);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_selector);
        getSharedPreferenceData();

        initializeViews();
    }

    void initializeViews() {
        back = (ImageButton) findViewById(R.id.back);
        levelOne=(TextView)findViewById(R.id.level_view_one);
        String levelOneText="প্রাক প্রাথমিক   "+levelPreSchool;
        levelOne.setText(levelOneText);
        levelTwo=(TextView)findViewById(R.id.level_view_two);
        levelThree=(TextView)findViewById(R.id.level_view_three);
        levelFour=(TextView)findViewById(R.id.level_view_four);
        levelFive=(TextView)findViewById(R.id.level_view_five);
        //toolbar = (Toolbar) findViewById(R.id.toolBar);
        level = (TextView) findViewById(R.id.level);
        refresh = (ImageButton) findViewById(R.id.refresh);
        //menu = (ImageButton) findViewById(R.id.menu);

        back.setOnClickListener(toolbarClickListener);
        //setSupportActionBar(toolbar);
//        level.setOnClickListener(toolbarClickListener);
        //      refresh.setOnClickListener(toolbarClickListener);
        //menu.setOnClickListener(toolbarClickListener);

        //refresh.setVisibility(View.INVISIBLE);
        level.setText(getString(R.string.headerLevelSelector));
    }
    void getSharedPreferenceData(){
        sobdokolpoPreference=getSharedPreferences(getString(R.string.sobdokolpoPreference),MODE_PRIVATE);
        levelPreSchool=sobdokolpoPreference.getInt(getString(R.string.levelPreSchool),0);
        levelPrimary=sobdokolpoPreference.getInt(getString(R.string.levelPrimary),0);
        levelJunior=sobdokolpoPreference.getInt(getString(R.string.levelJunior),0);
        levelSecondary=sobdokolpoPreference.getInt(getString(R.string.levelSecondary),0);
        levelAdvance=sobdokolpoPreference.getInt(getString(R.string.levelAdvance),0);
    }

    View.OnClickListener toolbarClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.back:
                    onBackPressed();
                    break;
            }
        }
    };

    // if user selects any level, this method will be called
    public void selectLevel(View view) {
        //SharedPreferences sharedPreferences=getSharedPreferences(getString(R.string.sobdokolpoPreference),MODE_PRIVATE);
        Intent intent = new Intent(this, LevelIndicatorActivity.class);
        switch (view.getId()) {
            case R.id.level_one:
                //int levelPreSchool=sharedPreferences.getInt(getString(R.string.levelPreSchool),0);
                intent.putExtra(getString(R.string.gameLevelName),getString(R.string.levelPreSchool));
                //intent.putExtra(getString(R.string.levelPreSchool),levelPreSchool);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_enter_top_bottom,R.anim.slide_exit_top_bottom);
                finish();
                break;

            case R.id.level_two:
                //int levelPrimary=sharedPreferences.getInt(getString(R.string.levelPrimary),0);

                intent.putExtra(getString(R.string.gameLevelName),getString(R.string.levelPrimary));
                //intent.putExtra(getString(R.string.gameLevelScore),levelPrimary);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_enter_top_bottom,R.anim.slide_exit_top_bottom);
                finish();
                break;
            case R.id.level_three:
                //int levelJunior=sharedPreferences.getInt(getString(R.string.levelJunior),0);
                intent.putExtra(getString(R.string.gameLevelName),getString(R.string.levelJunior));
                //intent.putExtra(getString(R.string.gameLevelScore),levelJunior);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_enter_top_bottom,R.anim.slide_exit_top_bottom);
                break;
            case R.id.level_four:
                //int levelSecondary=sharedPreferences.getInt(getString(R.string.levelSecondary),0);
                intent.putExtra(getString(R.string.gameLevelName),getString(R.string.levelSecondary));
                //intent.putExtra(getString(R.string.gameLevelScore),levelSecondary);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_enter_top_bottom,R.anim.slide_exit_top_bottom);
                break;
            case R.id.level_five:
                //int levelAdvance=sharedPreferences.getInt(getString(R.string.levelAdvance),0);
                intent.putExtra(getString(R.string.gameLevelName),getString(R.string.levelAdvance));
                //intent.putExtra(getString(R.string.gameLevelScore),levelAdvance);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_enter_top_bottom,R.anim.slide_exit_top_bottom);
                break;

        }
    }
}
