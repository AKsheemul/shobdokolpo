package com.middaydreamz.shobdokolpo;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.MyViewHolder>
        implements ItemTouchHelperAdapter {
    boolean allFillableRight=true;
    boolean allFillableLeft=true;
    boolean allFillableTop=true;
    boolean allFillableBottom=true;
    boolean allMatchRight=true;
    boolean allMatchLeft=true;
    boolean allMatchTop=true;
    boolean allMatchBottom=true;
    int countRight=0;
    int countLeft=0;
    int countTop=0;
    int countBottom=0;

    private List<DataContainer> list;
    //View sourceView;
    ArrayList<String> viewHolderList;
    ArrayList<String> solutionList;
    ArrayList<CustomTextView> TextViewList;
    ArrayList<Integer> colorFlag;
    Context context;
    Activity activity;
    FrameLayout frameLayoutRoot;
    Button buttonNext;
    int currentLevel;
    String currentGameLevelName;
    int currentLevelBest;
    final int FIXED=1;
    final int BLANK=3;
    final int DRAGABLE=2;
    final int FILLABLE=0;
    Drawable dBlank;
    SharedPreferences sp;
    Intent currentIntent;


    public RVAdapter(List<DataContainer> list, Activity activity) {
        this.list = list;
        //this.onStartDrag = onStartDrag;
        this.activity=activity;
        this.context=this.activity.getApplicationContext();
        viewHolderList = new ArrayList<String>();
        solutionList = new ArrayList<String>();
        TextViewList = new ArrayList<CustomTextView>();
        colorFlag = new ArrayList<Integer>();
        Log.d("RVAdapter constractor", "");
        currentLevel=this.activity.getIntent().getIntExtra("level",0);
        dBlank= ContextCompat.getDrawable(activity.getApplicationContext(),R.drawable.grid_blank);
        getIntentData();



    }

    OnStartDragListener onStartDrag;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.items, parent, false);


        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        DataContainer aTemp = list.get(position);
        //String aData = list.get(position);
        String aData = aTemp.getPuzzleData();
        String gridData;
        if(aData.trim().equals("#")){
            gridData="";
        }
        else gridData=aData;

        String solution = aTemp.getSolutionData();
        //int backgroundColorValue = aTemp.getColorBackGround();
        Drawable backgroundDrawable=aTemp.getDrawableBackground();
        int contentColorValue = aTemp.getColorContent();
        //holder.layout.setColorBackground(backgroundColorValue);
        holder.layout.setDrawableBackground(backgroundDrawable);
        holder.layout.setColorContent(contentColorValue);

       /* holder.data.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEventCompat.getActionMasked(event) ==
                        MotionEvent.ACTION_DOWN) {
                    onStartDrag.onStartDrag(holder);
                }
                return false;
            }
        });*/
        //holder.data.setTag("position"+position);
        holder.data.setTag("tag" + position);

        holder.data.setPosition(position);
        holder.layout.setPosition(position);

        holder.data.setFlag(aTemp.getFlag());
        holder.layout.setFlag(aTemp.getFlag());
        //holder.data.setText(aData);
        holder.data.setText(gridData);
        holder.data.setBackgroundDrawable(backgroundDrawable);
        holder.data.setTextColor(contentColorValue);
        holder.layout.setTag("position" + position);
        //holder.setData(aData, position);
        if (aTemp.getFlag() == 2) {
            //holder.data.setOnLongClickListener(new MyClickListener());
            //holder.data.setOnClickListener(new MyClickListener());
            holder.data.setOnTouchListener(new MyClickListener());

        }
        holder.data.setOnDragListener(new MyDragListener());
        viewHolderList.add(aData);
        solutionList.add(solution);
        TextViewList.add(holder.data);
        //colorFlag.add(colorValue);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        CustomTextView data;
        CustomLinearLayout layout;

        public MyViewHolder(View itemView) {
            super(itemView);

            data = (CustomTextView) itemView.findViewById(R.id.data);
            layout = (CustomLinearLayout) itemView.findViewById(R.id.layout_list);
        }

        public String getData() {
            return data.getText().toString();
        }

    }

    @Override
    public void onItemDismiss(int position) {
        list.remove(position);
        notifyItemRemoved(position);
    }


    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Log.d("OnItemMove", "InRVAdapter");
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(list, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(list, i, i - 1);
            }
            Collections.swap(list, fromPosition, toPosition);
            notifyItemMoved(fromPosition, toPosition);
            return true;
        }
        return false;
    }

    public class MyClickListener implements View.OnLongClickListener, View.OnClickListener, View.OnTouchListener {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
          /* CustomTextView customTextView=(CustomTextView) view;
           Log.d("Click","view from click "+view.getTag());
           ClipData.Item item=new ClipData.Item((CharSequence)customTextView.getTag());
           String[] minTypes={ClipDescription.MIMETYPE_TEXT_PLAIN};
           ClipData data= new ClipData(customTextView.getTag().toString(),minTypes,item);
           View.DragShadowBuilder shadowBuilder=new View.DragShadowBuilder(customTextView);
           customTextView.startDrag(data,shadowBuilder,customTextView,0);
           customTextView.setVisibility(View.VISIBLE);*/
            int x = (int) motionEvent.getX();
            int y = (int) motionEvent.getY();

            switch (motionEvent.getAction()) {

                case MotionEvent.ACTION_MOVE:
                    Log.i("Tag", "moving: (" + x + " , " + y + " )");
                    CustomTextView customTextView = (CustomTextView) view;
                    //customTextView.setBackgroundColor(Color.parseColor("#fff00f"));
                    Log.d("Click", "view from click " + view.getTag());
                    ClipData.Item item = new ClipData.Item((CharSequence) customTextView.getTag());
                    String[] minTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};
                    ClipData data = new ClipData(customTextView.getTag().toString(), minTypes, item);
                    View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(customTextView);
                    customTextView.startDrag(data, shadowBuilder, customTextView, 0);
                    CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
                    break;
                case MotionEvent.ACTION_DOWN:
                    int i = (int) motionEvent.getX();
                    int j = (int) motionEvent.getY();
                    Log.i("Tag", "moving: (" + i + " , " + j + " )");
            }
            return true;
            //return false;
        }

        @Override
        public void onClick(View view) {
            //final String gameLevel="com.middaydreamz.shobdokolpo.gameLevel";
            if(view.getId()==R.id.next){

                Intent intent=new Intent(activity.getApplicationContext(), SplashActivity.class);
                intent.putExtra("level",currentLevel+1);
                intent.putExtra(activity.getString(R.string.gameLevelName),currentGameLevelName);
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.slide_enter_top_bottom,R.anim.slide_exit_top_bottom);
                activity.finish();


            }
        }


        @Override
        public boolean onLongClick(View view) {
            //sourceView=view;
            CustomTextView customTextView = (CustomTextView) view;
            Log.d("Click", "view from click " + view.getTag());
            ClipData.Item item = new ClipData.Item((CharSequence) customTextView.getTag());
            String[] minTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};
            ClipData data = new ClipData(customTextView.getTag().toString(), minTypes, item);
            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(customTextView);
            customTextView.startDrag(data, shadowBuilder, customTextView, 0);
            customTextView.setVisibility(View.VISIBLE);
            return true;

        }
    }

    public class MyDragListener implements View.OnDragListener {

        @Override
        public boolean onDrag(View view, DragEvent dragEvent) {
            Log.d("drop", "drop" + view);
            switch (dragEvent.getAction()) {

                case DragEvent.ACTION_DRAG_STARTED:
                    Log.d("dragStarte", "DragStarted");
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    break;
                case DragEvent.ACTION_DROP:

                    /*********************************************************************/
                    CustomTextView destination = (CustomTextView) view;
                    //ColorDrawable colorDrawable=(ColorDrawable)destination.getBackground();
                    //int destinationColor=colorDrawable.getColor();
                    int destinationPosition = destination.getPosition();
                    //ViewGroup destinationParent=(ViewGroup) destination.getParent();
                    CustomLinearLayout destinationParent = (CustomLinearLayout) destination.getParent();
                    int destinationParentPosition = destinationParent.getPosition();
                    Drawable destinationParentBackgroundDrawable=destinationParent.getDrawableBackground();
                    int destinationParentBackgroundColor = destinationParent.getColorBackground();

                    int destinationParentContentColor = destinationParent.getColorContent();
                    Log.d("ParentPosition", "ParentPosition" + destination.getPosition());
                    CustomTextView source = (CustomTextView) dragEvent.getLocalState();
                    int sourcePosition = source.getPosition();
                    //TextView tt = (TextView) source;
                    CustomLinearLayout sourceParent = (CustomLinearLayout) source.getParent();
                    int sourceParentPosition = sourceParent.getPosition();
                    Drawable sourceParentBackgroundDrawable=sourceParent.getDrawableBackground();
                    int sourceParentBackgroundColor = sourceParent.getColorBackground();
                    int sourceParentContentColor = sourceParent.getColorContent();

                    Drawable d=destination.getBackground();
                    Log.d("Drawable ","Drawable "+(d==SplashActivity.drawableFillable));


                    Log.d("destinationTag", "destinationTag: " + destination.getTag());
                    if (source.getFlag() == 2 && (destination.getFlag() == 0 || destination.getFlag() == 3)) {

                        source.setPosition(destinationPosition);
                        destination.setPosition(sourcePosition);

                        //ColorDrawable colorDrawable = (ColorDrawable) source.getBackground();

                        //int sourceBackgroundColor = colorDrawable.getColor();
                        Drawable sourceBackgroundDrawable = source.getBackground();
                        //int tempColor=Color.parseColor("#ffffff");
                        //Log.d("Color ","Color"+(tempColor==sourceBackgroundColor));
                        //int colorBackgroundRed = Color.parseColor("#ff0000");
                        //int colorBackgroundGreen = Color.parseColor("#00ff00");
                        //if (sourceBackgroundColor == colorBackgroundGreen || sourceBackgroundColor == colorBackgroundRed) {
                        //removeColor(sourceBackgroundDrawable);
                        removeBackgroundColorIndividual(sourceBackgroundDrawable,sourcePosition);

                        source.setTextColor(destinationParentContentColor);
                        if (destinationParent.getFlag() == 2 || destinationParent.getFlag() == 3) {
                            source.setBackgroundDrawable(SplashActivity.drawableDragable);
                        } else {
                            source.setBackgroundDrawable(destinationParentBackgroundDrawable);
                        }
                        //source.setBackgroundColor(destinationParentBackgroundColor);
                        destination.setTextColor(sourceParentContentColor);
                        if (sourceParent.getFlag() == 2 || sourceParent.getFlag() == 3) {
                            destination.setBackgroundDrawable(SplashActivity.drawableBlank);
                        } else {
                            destination.setBackgroundDrawable(sourceParentBackgroundDrawable);
                        }


                        //destination.setBackgroundColor(sourceParentBackgroundColor);
                        Collections.swap(viewHolderList, sourcePosition, destinationPosition);
                        Collections.swap(TextViewList, sourcePosition, destinationPosition);

                        sourceParent.removeView(source);
                        destinationParent.removeView(destination);
                        destinationParent.addView(source);
                        sourceParent.addView(destination);

                        if (destinationParent.getFlag()==FILLABLE)
                            matchIndividualWord(destinationPosition);


                        boolean allFillable = true;

                        for (int i = 0; i < TextViewList.size(); i++) {
                            CustomTextView customTextView = TextViewList.get(i);
                            CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
                            if (customLinearLayout.getFlag() == 0) {
                                if (customTextView.getFlag() != 2) {
                                    allFillable = false;
                                    break;
                                }
                            }

                        }
                        if (allFillable) {
                            boolean solutionMatch = true;
                            for (int i = 0; i < TextViewList.size(); i++) {
                                CustomTextView customTextView = TextViewList.get(i);
                                CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
                                if (customLinearLayout.getFlag() == 0) {
                                    if (!viewHolderList.get(i).equals(solutionList.get(i))) {
                                        solutionMatch = false;
                                        break;
                                    }

                                }


                                Log.d("Text at position " + i, "Text at position " + i + " is " + viewHolderList.get(i));
                            }
                            if (solutionMatch) {
                                /*for (int i = 0; i < TextViewList.size(); i++) {
                                    CustomTextView customTextView = TextViewList.get(i);
                                    CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
                                    if (customLinearLayout.getFlag() == 1 || customLinearLayout.getFlag() == 0)
                                        //customTextView.setBackgroundColor(Color.parseColor("#00ff00"));
                                        customTextView.setBackgroundDrawable(SplashActivity.drawableSolution);
                                }*/
                                if(currentLevel==currentLevelBest){
                                    SharedPreferences.Editor editor = sp.edit();
                                    editor.putInt(currentGameLevelName, currentLevelBest + 1);
                                    editor.commit();
                                }


                                showNext();

                                Log.d("SolutionMatch", "SolutionMatch");
                            } else {
                                /*for (int i = 0; i < TextViewList.size(); i++) {
                                    CustomTextView customTextView = TextViewList.get(i);
                                    CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
                                    if (customLinearLayout.getFlag() == 1 || customLinearLayout.getFlag() == 0)
                                        customTextView.setBackgroundDrawable(SplashActivity.drawableWrong);
                                    //customTextView.setBackgroundColor(Color.parseColor("#ff0000"));
                                }*/
                                Log.d("SolutionIsNotMatch", "SolutionIsNotMatch");

                            }
                        }

                    }

                    break;
            }
            return true;
        }
    }
    void showNext(){
        frameLayoutRoot=(FrameLayout) activity.findViewById(R.id.nextRoot);
        buttonNext=(Button)activity.findViewById(R.id.next);
        buttonNext.startAnimation(AnimationUtils.loadAnimation(
                activity.getApplicationContext(),R.anim.left_right_animation));
        frameLayoutRoot.setVisibility(View.VISIBLE);
        frameLayoutRoot.setAlpha(0.8f);
        //frameLayoutRoot.setBackgroundColor(Color.parseColor("#ffffff"));
        frameLayoutRoot.setOnClickListener(new MyClickListener());
        buttonNext.setOnClickListener(new MyClickListener());
    }


    /*************************************/
    void matchIndividualWord(int position){
        allFillableRight=true;
        allFillableLeft=true;
        allFillableTop=true;
        allFillableBottom=true;
        allMatchRight=true;
        allMatchLeft=true;
        allMatchTop=true;
        allMatchBottom=true;
        countLeft=0;
        countRight=0;
        countTop=0;
        countBottom=0;

        int counterLeft=position%6;
        int counterRight=5-counterLeft;
        //checkRightGrids(position);
        //checkLeftGrids(position);
        //checkBottomGrids(position);
        //checkTopGrids(position);
        //makeBottomTopGridGreenRed(position);
        //makeRightLeftGridGreenRed(position);


        //Checking all right index
        for(int i=position;i<=position+counterRight;i++){
            if(solutionList.get(i).equals("#"))
                break;

            countRight++;
            CustomTextView customTextView = TextViewList.get(i);
            CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
            if (customLinearLayout.getFlag() == 0) {
                if (!viewHolderList.get(i).equals(solutionList.get(i))) {
                    allMatchRight = false;
                }
                if (customTextView.getFlag() != 2) {
                    allFillableRight = false;
                }

            }

        }
        //Checking all left index
        for(int i=position-1;i>=position-counterLeft;i--){
            if(solutionList.get(i).equals("#"))
                break;
            countLeft++;
            CustomTextView customTextView = TextViewList.get(i);
            CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
            if (customLinearLayout.getFlag() == 0) {
                if (!viewHolderList.get(i).equals(solutionList.get(i))) {
                    allMatchLeft = false;
                }
                if (customTextView.getFlag() != 2) {
                    allFillableLeft = false;
                }

            }

        }

        //Checking all bottom index
        for(int i=position;i<solutionList.size();i=i+6){
            if(solutionList.get(i).trim().equals("#"))
                break;
            countBottom++;
            CustomTextView customTextView = TextViewList.get(i);
            CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
            if (customLinearLayout.getFlag() == 0) {
                if (!viewHolderList.get(i).equals(solutionList.get(i))) {
                    allMatchBottom = false;
                }
                if (customTextView.getFlag() != 2) {
                    allFillableBottom = false;
                }

            }

        }

        //Checking all top index
        for(int i=position-6;i>=0;i=i-6){
            if(solutionList.get(i).equals("#"))
                break;
            countTop++;
            CustomTextView customTextView = TextViewList.get(i);
            CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
            if (customLinearLayout.getFlag() == 0) {
                if (!viewHolderList.get(i).equals(solutionList.get(i))) {
                    allMatchTop = false;
                }
                if (customTextView.getFlag() != 2) {
                    allFillableTop = false;
                }

            }

        }

/*************************************************************************************/

        if(allFillableBottom&&allFillableTop&&(countTop+countBottom>=2)){
            if(allMatchTop&&allMatchBottom){
                //making center to all bottom grid green
                for(int i=position;i<solutionList.size();i=i+6){
                    if(solutionList.get(i).trim().equals("#"))
                        break;
                    CustomTextView customTextView = TextViewList.get(i);
                    CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
                    int te=customLinearLayout.getFlag();
                    if (customLinearLayout.getFlag() == 1 || customLinearLayout.getFlag() == 0){
                        //customTextView.setBackgroundColor(Color.parseColor("#00ff00"));
                        customTextView.setBackgroundDrawable(SplashActivity.drawableSolution);
                        customTextView.setOnTouchListener(null);
                        customTextView.setOnDragListener(null);
                    }


                }
                //Checking all top index
                for(int i=position-6;i>=0;i=i-6){
                    if(solutionList.get(i).equals("#"))
                        break;
                    CustomTextView customTextView = TextViewList.get(i);
                    CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
                    if (customLinearLayout.getFlag() == 1 || customLinearLayout.getFlag() == 0){
                        //customTextView.setBackgroundColor(Color.parseColor("#00ff00"));
                        customTextView.setBackgroundDrawable(SplashActivity.drawableSolution);
                        customTextView.setOnTouchListener(null);
                        customTextView.setOnDragListener(null);
                    }

                }

            }
            else{
                //making center to all bottom grid green
                for(int i=position;i<solutionList.size();i=i+6){
                    if(solutionList.get(i).trim().equals("#"))
                        break;
                    CustomTextView customTextView = TextViewList.get(i);
                    CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
                    int te=customLinearLayout.getFlag();
                    if (customLinearLayout.getFlag() == 1 || customLinearLayout.getFlag() == 0){
                        //customTextView.setBackgroundColor(Color.parseColor("#00ff00"));
                        if(customTextView.getBackground()==SplashActivity.drawableSolution)
                            continue;
                        customTextView.setBackgroundDrawable(SplashActivity.drawableWrong);
                    }


                }
                //Checking all top index
                for(int i=position-6;i>=0;i=i-6){
                    if(solutionList.get(i).equals("#"))
                        break;
                    CustomTextView customTextView = TextViewList.get(i);
                    CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
                    if (customLinearLayout.getFlag() == 1 || customLinearLayout.getFlag() == 0){
                        //customTextView.setBackgroundColor(Color.parseColor("#00ff00"));
                        if(customTextView.getBackground()==SplashActivity.drawableSolution)
                            continue;
                        customTextView.setBackgroundDrawable(SplashActivity.drawableWrong);
                    }

                }

            }
        }
        //**********************************************************************************************//*
        if(allFillableLeft&&allFillableRight&&(countLeft+countRight>=2)){
            if(allMatchLeft&&allMatchRight){
                //making center to all right grid green
                for(int i=position;i<=position+counterRight;i++){
                    if(solutionList.get(i).trim().equals("#"))
                        break;
                    CustomTextView customTextView = TextViewList.get(i);
                    CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
                    int te=customLinearLayout.getFlag();
                    if (customLinearLayout.getFlag() == 1 || customLinearLayout.getFlag() == 0){
                        //customTextView.setBackgroundColor(Color.parseColor("#00ff00"));
                        customTextView.setBackgroundDrawable(SplashActivity.drawableSolution);
                        customTextView.setOnTouchListener(null);
                        customTextView.setOnDragListener(null);
                    }


                }
                //making all left grid green
                for(int i=position-1;i>=position-counterLeft;i--){
                    if(solutionList.get(i).equals("#"))
                        break;
                    CustomTextView customTextView = TextViewList.get(i);
                    CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
                    if (customLinearLayout.getFlag() == 1 || customLinearLayout.getFlag() == 0){
                        //customTextView.setBackgroundColor(Color.parseColor("#00ff00"));
                        customTextView.setBackgroundDrawable(SplashActivity.drawableSolution);
                        customTextView.setOnTouchListener(null);
                        customTextView.setOnDragListener(null);
                    }

                }
            }
            else{
                //making center to all right grid red
                for(int i=position;i<=position+counterRight;i++){
                    if(solutionList.get(i).trim().equals("#"))
                        break;
                    CustomTextView customTextView = TextViewList.get(i);
                    CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
                    int te=customLinearLayout.getFlag();
                    if (customLinearLayout.getFlag() == 1 || customLinearLayout.getFlag() == 0){
                        //customTextView.setBackgroundColor(Color.parseColor("#00ff00"));
                        if(customTextView.getBackground()==SplashActivity.drawableSolution)
                            continue;
                        customTextView.setBackgroundDrawable(SplashActivity.drawableWrong);
                    }


                }
                //making all left grid red
                for(int i=position-1;i>=position-counterLeft;i--){
                    if(solutionList.get(i).equals("#"))
                        break;
                    CustomTextView customTextView = TextViewList.get(i);
                    CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
                    if (customLinearLayout.getFlag() == 1 || customLinearLayout.getFlag() == 0){
                        //customTextView.setBackgroundColor(Color.parseColor("#00ff00"));
                        if(customTextView.getBackground()==SplashActivity.drawableSolution)
                            continue;
                        customTextView.setBackgroundDrawable(SplashActivity.drawableWrong);
                    }

                }
            }
        }


        Log.d("IndividualMatch", "Individual match row "+(allMatchLeft&&allMatchRight));
        Log.d("IndividualMatch", "Individual match column "+(allMatchBottom&&allMatchTop));




    }
    void removeColor(Drawable sourceBackgroundDrawable){
        if (sourceBackgroundDrawable==SplashActivity.drawableSolution || sourceBackgroundDrawable==SplashActivity.drawableWrong) {
            for (int i = 0; i < TextViewList.size(); i++) {
                CustomTextView custView = TextViewList.get(i);
                CustomLinearLayout custLinLayout = (CustomLinearLayout) custView.getParent();
                int custViewBackgroundColr = custLinLayout.getColorBackground();
                Drawable drawableBackGround=custLinLayout.getDrawableBackground();
                int custViewContentColor = custLinLayout.getColorContent();

                //if((custLinLayout.getFlag()==BLANK||custLinLayout.getFlag()==DRAGABLE)){
                if(custView.getText().equals("")&&(custLinLayout.getFlag()==BLANK||custLinLayout.getFlag()==DRAGABLE)){
                    custView.setBackgroundDrawable(SplashActivity.drawableBlank);
                }
                else custView.setBackgroundDrawable(drawableBackGround);
                //custView.setBackgroundColor(custViewBackgroundColr);
                custView.setTextColor(custViewContentColor);
            }
        }

    }
    void removeBackgroundColorIndividual(Drawable sourceBackgroundDrawable, int position){
        if (sourceBackgroundDrawable==SplashActivity.drawableSolution || sourceBackgroundDrawable==SplashActivity.drawableWrong) {
            removeColorFromRightGrids(position);
            removeColorFromLeftGrids(position);
            removeColorFromTopGrids(position);
            removeColorFromBottomGrids(position);

        }

    }
    void checkRightGrids(int position){
        int counterLeft=position%6;
        int counterRight=5-counterLeft;
        //Checking all right index
        for(int i=position;i<=position+counterRight;i++){
            if(solutionList.get(i).equals("#"))
                break;
            CustomTextView customTextView = TextViewList.get(i);
            CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
            if (customLinearLayout.getFlag() == 0) {
                if (!viewHolderList.get(i).equals(solutionList.get(i))) {
                    allMatchRight = false;
                }
                if (customTextView.getFlag() != 2) {
                    allFillableRight = false;
                }

            }

        }
    }
    void checkLeftGrids(int position){
        int counterLeft=position%6;
        //Checking all left index
        for(int i=position-1;i>=position-counterLeft;i--){
            if(solutionList.get(i).equals("#"))
                break;
            CustomTextView customTextView = TextViewList.get(i);
            CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
            if (customLinearLayout.getFlag() == 0) {
                if (!viewHolderList.get(i).equals(solutionList.get(i))) {
                    allMatchLeft = false;
                }
                if (customTextView.getFlag() != 2) {
                    allFillableLeft = false;
                }

            }

        }

    }
    void checkBottomGrids(int position){
        for(int i=position;i<solutionList.size();i=i+6){
            if(solutionList.get(i).trim().equals("#"))
                break;
            CustomTextView customTextView = TextViewList.get(i);
            CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
            if (customLinearLayout.getFlag() == 0) {
                if (!viewHolderList.get(i).equals(solutionList.get(i))) {
                    allMatchBottom = false;
                }
                if (customTextView.getFlag() != 2) {
                    allFillableBottom = false;
                }

            }

        }


    }
    void checkTopGrids(int position){
        for(int i=position-6;i>=0;i=i-6){
            if(solutionList.get(i).equals("#"))
                break;
            CustomTextView customTextView = TextViewList.get(i);
            CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
            if (customLinearLayout.getFlag() == 0) {
                if (!viewHolderList.get(i).equals(solutionList.get(i))) {
                    allMatchTop = false;
                }
                if (customTextView.getFlag() != 2) {
                    allFillableTop = false;
                }

            }

        }

    }

    void removeColorFromRightGrids(int position){
        int counterLeft=position%6;
        int counterRight=5-counterLeft;
        for(int i=position;i<=position+counterRight;i++){
            if (solutionList.get(i).equals("#"))
                break;
            CustomTextView custView = TextViewList.get(i);
            /*int viewPos=custView.getPosition();
            int temp1=i-6;
            int temp2=i+6;
            CustomTextView custViewTop=TextViewList.get(temp1);
            CustomTextView custViewBottom=TextViewList.get(temp2);*/
            boolean topBottmFlag=isTopBottomSameColor(i);
            if((topBottmFlag && custView.getBackground()==SplashActivity.drawableWrong)|| custView.getBackground()==SplashActivity.drawableSolution)
                continue;
            CustomLinearLayout custLinLayout = (CustomLinearLayout) custView.getParent();
            int custViewBackgroundColr = custLinLayout.getColorBackground();
            Drawable drawableBackGround=custLinLayout.getDrawableBackground();
            int custViewContentColor = custLinLayout.getColorContent();
            /*if((custViewBottom.getBackground()==SplashActivity.drawableSolution)||(custViewTop.getBackground()==SplashActivity.drawableSolution)){
                continue;
            }*/
            /*if(custView.getBackground()==SplashActivity.drawableSolution)
                continue;*/
            /*if((isTopSameColor(viewPos)&&isBottomSameColor(viewPos))||(isLeftSameColor(viewPos)&&isRightSameColor(viewPos)))
                continue;*/

            custView.setBackgroundDrawable(drawableBackGround);
            //custView.setBackgroundColor(custViewBackgroundColr);
            custView.setTextColor(custViewContentColor);

        }

    }
    void removeColorFromLeftGrids(int position){
        int counterLeft=position%6;
        //Checking all left index
        for(int i=position-1;i>=position-counterLeft;i--){
            if(solutionList.get(i).equals("#"))
                break;
            CustomTextView custView = TextViewList.get(i);
           /* CustomTextView custViewTop=TextViewList.get(i-6);
            CustomTextView custViewBottom=TextViewList.get(i+6);
            int viewPos=custView.getPosition();*/
            boolean topBottmFlag=isTopBottomSameColor(i);
            if((topBottmFlag && custView.getBackground()==SplashActivity.drawableWrong)|| custView.getBackground()==SplashActivity.drawableSolution)
                continue;

            CustomLinearLayout custLinLayout = (CustomLinearLayout) custView.getParent();
            int custViewBackgroundColr = custLinLayout.getColorBackground();
            Drawable drawableBackGround=custLinLayout.getDrawableBackground();
            int custViewContentColor = custLinLayout.getColorContent();
            /*if((custViewBottom.getBackground()==SplashActivity.drawableSolution)||(custViewTop.getBackground()==SplashActivity.drawableSolution)){
                continue;
            }*/
            /*if(custView.getBackground()==SplashActivity.drawableSolution)
                continue;*/
            /*if((isTopSameColor(viewPos)&&isBottomSameColor(viewPos))||(isLeftSameColor(viewPos)&&isRightSameColor(viewPos)))
                continue;*/
            custView.setBackgroundDrawable(drawableBackGround);
            //custView.setBackgroundColor(custViewBackgroundColr);
            custView.setTextColor(custViewContentColor);


        }

    }
    void removeColorFromBottomGrids(int position){
        //Checking all bottom index
        for(int i=position;i<solutionList.size();i=i+6){
            if(solutionList.get(i).trim().equals("#"))
                break;
            CustomTextView custView = TextViewList.get(i);
           // boolean leftSameColorFlag=isLeftSameColor(i-1);
            //boolean rightSameColorFlag=isRightSameColor(i+1);
           /* CustomTextView custViewLeft=TextViewList.get(i-1);
            CustomTextView custViewRight=TextViewList.get(i+1);
            int viewPos=custView.getPosition();*/
            boolean leftRightFlag=isLeftRightSameColor(i);
            if((leftRightFlag && custView.getBackground()==SplashActivity.drawableWrong)|| custView.getBackground()==SplashActivity.drawableSolution)
                continue;

            CustomLinearLayout custLinLayout = (CustomLinearLayout) custView.getParent();
            int custViewBackgroundColr = custLinLayout.getColorBackground();
            Drawable drawableBackGround=custLinLayout.getDrawableBackground();
            int custViewContentColor = custLinLayout.getColorContent();

            /*if((custViewLeft.getBackground()==SplashActivity.drawableSolution)||(custViewRight.getBackground()==SplashActivity.drawableSolution)){
                continue;
            }*/
           /* if(custView.getBackground()==SplashActivity.drawableSolution)
                continue;*/
            /*if((isTopSameColor(viewPos)&&isBottomSameColor(viewPos))||(isLeftSameColor(viewPos)&&isRightSameColor(viewPos)))
                continue;*/
            custView.setBackgroundDrawable(drawableBackGround);
            //custView.setBackgroundColor(custViewBackgroundColr);
            custView.setTextColor(custViewContentColor);

        }
    }
    void removeColorFromTopGrids(int position){
        for(int i=position-6;i>=0;i=i-6){
            if(solutionList.get(i).equals("#"))
                break;
            CustomTextView custView = TextViewList.get(i);
            /*CustomTextView custViewLeft=TextViewList.get(i-1);
            CustomTextView custViewRight=TextViewList.get(i+1);
            int viewPos=custView.getPosition();*/
            boolean leftRightFlag=isLeftRightSameColor(i);
            if((leftRightFlag && custView.getBackground()==SplashActivity.drawableWrong)|| custView.getBackground()==SplashActivity.drawableSolution)
                continue;
            CustomLinearLayout custLinLayout = (CustomLinearLayout) custView.getParent();
            int custViewBackgroundColr = custLinLayout.getColorBackground();
            Drawable drawableBackGround=custLinLayout.getDrawableBackground();
            int custViewContentColor = custLinLayout.getColorContent();
            /*if((custViewLeft.getBackground()==SplashActivity.drawableSolution)||(custViewRight.getBackground()==SplashActivity.drawableSolution)){
                continue;
            }*/

            /*if((isTopSameColor(viewPos)&&isBottomSameColor(viewPos))||(isLeftSameColor(viewPos)&&isRightSameColor(viewPos)))
                continue;*/
            custView.setBackgroundDrawable(drawableBackGround);
            //custView.setBackgroundColor(custViewBackgroundColr);
            custView.setTextColor(custViewContentColor);

        }
    }
    void makeBottomTopGridGreenRed(int position){
        if(allFillableBottom&&allFillableTop){
            if(allMatchTop&&allMatchBottom){
                //making center to all bottom grid green
                for(int i=position;i<solutionList.size();i=i+6){
                    if(solutionList.get(i).trim().equals("#"))
                        break;
                    CustomTextView customTextView = TextViewList.get(i);
                    CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
                    int te=customLinearLayout.getFlag();
                    if (customLinearLayout.getFlag() == 1 || customLinearLayout.getFlag() == 0){
                        //customTextView.setBackgroundColor(Color.parseColor("#00ff00"));
                        customTextView.setBackgroundDrawable(SplashActivity.drawableSolution);
                    }


                }
                //Checking all top index
                for(int i=position-6;i>=0;i=i-6){
                    if(solutionList.get(i).equals("#"))
                        break;
                    CustomTextView customTextView = TextViewList.get(i);
                    CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
                    if (customLinearLayout.getFlag() == 1 || customLinearLayout.getFlag() == 0){
                        //customTextView.setBackgroundColor(Color.parseColor("#00ff00"));
                        customTextView.setBackgroundDrawable(SplashActivity.drawableSolution);
                    }

                }

            }
            else{
                //making center to all bottom grid green
                for(int i=position;i<solutionList.size();i=i+6){
                    if(solutionList.get(i).trim().equals("#"))
                        break;
                    CustomTextView customTextView = TextViewList.get(i);
                    CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
                    int te=customLinearLayout.getFlag();
                    if (customLinearLayout.getFlag() == 1 || customLinearLayout.getFlag() == 0){
                        //customTextView.setBackgroundColor(Color.parseColor("#00ff00"));
                        customTextView.setBackgroundDrawable(SplashActivity.drawableWrong);
                    }


                }
                //Checking all top index
                for(int i=position-6;i>=0;i=i-6){
                    if(solutionList.get(i).equals("#"))
                        break;
                    CustomTextView customTextView = TextViewList.get(i);
                    CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
                    if (customLinearLayout.getFlag() == 1 || customLinearLayout.getFlag() == 0){
                        //customTextView.setBackgroundColor(Color.parseColor("#00ff00"));
                        customTextView.setBackgroundDrawable(SplashActivity.drawableWrong);
                    }

                }

            }
        }

    }
    void makeRightLeftGridGreenRed(int position){
        int counterLeft=position%6;
        int counterRight=5-counterLeft;
        if(allFillableLeft&&allFillableRight){
            if(allMatchLeft&&allMatchRight){
                //making center to all right grid green
                for(int i=position;i<=position+counterRight;i++){
                    if(solutionList.get(i).trim().equals("#"))
                        break;
                    CustomTextView customTextView = TextViewList.get(i);
                    CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
                    int te=customLinearLayout.getFlag();
                    if (customLinearLayout.getFlag() == 1 || customLinearLayout.getFlag() == 0){
                        //customTextView.setBackgroundColor(Color.parseColor("#00ff00"));
                        customTextView.setBackgroundDrawable(SplashActivity.drawableSolution);
                    }


                }
                //making all left grid green
                for(int i=position-1;i>=position-counterLeft;i--){
                    if(solutionList.get(i).equals("#"))
                        break;
                    CustomTextView customTextView = TextViewList.get(i);
                    CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
                    if (customLinearLayout.getFlag() == 1 || customLinearLayout.getFlag() == 0){
                        //customTextView.setBackgroundColor(Color.parseColor("#00ff00"));
                        customTextView.setBackgroundDrawable(SplashActivity.drawableSolution);
                    }

                }
            }
            else{
                //making center to all right grid red
                for(int i=position;i<=position+counterRight;i++){
                    if(solutionList.get(i).trim().equals("#"))
                        break;
                    CustomTextView customTextView = TextViewList.get(i);
                    CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
                    int te=customLinearLayout.getFlag();
                    if (customLinearLayout.getFlag() == 1 || customLinearLayout.getFlag() == 0){
                        //customTextView.setBackgroundColor(Color.parseColor("#00ff00"));
                        customTextView.setBackgroundDrawable(SplashActivity.drawableWrong);
                    }


                }
                //making all left grid red
                for(int i=position-1;i>=position-counterLeft;i--){
                    if(solutionList.get(i).equals("#"))
                        break;
                    CustomTextView customTextView = TextViewList.get(i);
                    CustomLinearLayout customLinearLayout = (CustomLinearLayout) customTextView.getParent();
                    if (customLinearLayout.getFlag() == 1 || customLinearLayout.getFlag() == 0){
                        //customTextView.setBackgroundColor(Color.parseColor("#00ff00"));
                        customTextView.setBackgroundDrawable(SplashActivity.drawableWrong);
                    }

                }
            }
        }

    }
    void test(){

    }
   boolean isRightSameColor(int position){
       int counterLeft=position%6;
       int counterRight=5-counterLeft;
       CustomTextView ViewAtPosition=TextViewList.get(position);
       for(int i=position;i<=position+counterRight;i++){
           if(solutionList.get(i).trim().equals("#"))
               break;
           CustomTextView customTextView = TextViewList.get(i);
           if(ViewAtPosition.getBackground()!=customTextView.getBackground())
               return false;
       }
       return true;
   }
   boolean isLeftSameColor(int position){
       int counterLeft=position%6;
       int counterRight=5-counterLeft;
       CustomTextView ViewAtPosition=TextViewList.get(position);
       for(int i=position-1;i>=position-counterLeft;i--){
           if(solutionList.get(i).trim().equals("#"))
               break;
           CustomTextView customTextView = TextViewList.get(i);
           if(ViewAtPosition.getBackground()!=customTextView.getBackground())
               return false;
       }
       return true;
   }
   boolean isTopSameColor(int position){
       CustomTextView ViewAtPosition=TextViewList.get(position);
       for(int i=position-6;i>=0;i=i-6){
           if(solutionList.get(i).trim().equals("#"))
               break;
           CustomTextView customTextView = TextViewList.get(i);
           if(ViewAtPosition.getBackground()!=customTextView.getBackground())
               return false;
       }
       return true;

   }
   boolean isBottomSameColor(int position){
       CustomTextView ViewAtPosition=TextViewList.get(position);
       for(int i=position;i<solutionList.size();i=i+6){
           if(solutionList.get(i).trim().equals("#"))
               break;
           CustomTextView customTextView = TextViewList.get(i);
           if(ViewAtPosition.getBackground()!=customTextView.getBackground())
               return false;
       }
       return true;
   }
    boolean isLeftRightSameColor(int position){
        boolean leftFlag=true;
        boolean rightFlag=true;
        int leftCount=0;
        int rightCount=0;
        Log.e("In isLeftRightSameColor","L R "+(position-1)+" "+(position+1));
        int inRowPositionLeft=(position-1)%6;
        if(inRowPositionLeft!=5){
            for(int i=position-1;i>=position-inRowPositionLeft-1;i--){

                CustomTextView custTextView=TextViewList.get(i);
                if(solutionList.get(i).trim().equals("#")) break;
                leftCount++;
                if(custTextView.getBackground()!=SplashActivity.drawableWrong && custTextView.getBackground()!=SplashActivity.drawableSolution) {
                    leftFlag=false;
                    break;
                }
            }
        }

        int inRowPositionRight=(position+1)%6;
        if(inRowPositionRight!=0){

            for(int i=position+1;i<=position+(5-inRowPositionRight)+1;i++){

                CustomTextView custTextView=TextViewList.get(i);
                if(solutionList.get(i).trim().equals("#")) break;
                rightCount++;
                if(custTextView.getBackground()!=SplashActivity.drawableWrong &&custTextView.getBackground()!=SplashActivity.drawableSolution){
                    rightFlag= false;
                    break;
                }
            }
        }
        //Log.d("leftCount "+leftCount," rightCount "+rightCount);
        //if(leftCount==0 && rightCount==0) return false;
        if(rightFlag && leftFlag &&(leftCount!=0 || rightCount!=0))
            return true;
        return false;
    }

    boolean isTopBottomSameColor(int position){
        boolean topFlag=true;
        boolean bottomFlag=true;
        int topCount=0;
        int bottomCount=0;

        Log.d("In isTopBottomSameColor","T B "+(position-6)+" "+(position+6));
        if(position-6>=0){
            for(int i=position-6;i>=0;i-=6){

                CustomTextView custTextView=TextViewList.get(i);
                if(solutionList.get(i).trim().equals("#")) break;
                topCount++;
                if(custTextView.getBackground()!=SplashActivity.drawableWrong && custTextView.getBackground()!=SplashActivity.drawableSolution) {
                    topFlag= false;
                    break;
                }
            }
        }

        if(position+6<=41){
            for(int i=position+6;i<=41;i+=6){

                CustomTextView custTextView=TextViewList.get(i);
                if(solutionList.get(i).trim().equals("#")) break;
                bottomCount++;
                if(custTextView.getBackground()!=SplashActivity.drawableWrong && custTextView.getBackground()!=SplashActivity.drawableSolution){
                    bottomFlag=false;
                    break;
                }
            }
        }
        //Log.d("topCount "+topCount," bottomCount "+bottomCount);
        //if(topCount==0 && bottomCount==0) return false;
        if(topFlag && bottomFlag && (topCount!=0 || bottomCount!=0))
            return true;
        return false;
    }
    void getIntentData(){
        sp=activity.getSharedPreferences(activity.getString(R.string.sobdokolpoPreference),Context.MODE_PRIVATE);
        currentIntent=activity.getIntent();
        currentLevel=currentIntent.getIntExtra("level",1);
        currentGameLevelName=currentIntent.getStringExtra(activity.getString(R.string.gameLevelName));
        currentLevelBest=sp.getInt(currentGameLevelName,1);

    }

}

