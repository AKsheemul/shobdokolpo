package com.middaydreamz.shobdokolpo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LevelIndicatorActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    LevelAdapter levelAdapter;
    ArrayList<Integer>list;
    SharedPreferences sharedPreferences;
    TextView toolbarLevel;
    ImageButton backButton;
    ImageButton refreshButton;
    String gameLevelName;
    String gameLevelScore;
    String levelPreSchool;
    String levelPrimary;
    String levelJunior;
    String levelSecondary;
    String levelAdvance;
    String solutionPreSchool;
    String solutionPrimary;
    String solutionJunior;
    String solutionSecondary;
    String solutionAdvance;
    String modelPreSchool;
    String modelPrimary;
    String modelJunior;
    String modelSecondary;
    String modelAdvance;

    public static HashMap<String,String> modelFileName;
    public static HashMap<String,String>solutionFileName;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(this, LevelSelectorActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_enter_bottom_up,R.anim.slide_exit_bottom_up);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_indicator);
        getIntentData();
        initializeView();
        initializeStringResource();
        storeModelData();
        storeSolutionData();



        /*sharedPreferences=getSharedPreferences(getString(R.string.sharedPreference), Context.MODE_PRIVATE);
        int gameLevelFlag=sharedPreferences.getInt(getString(R.string.gameLevel),0);

        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putInt(getString(R.string.gameLevel),gameLevelFlag);
        editor.commit();*/

        recyclerView=(RecyclerView)findViewById(R.id.gameLevel);
        list=new ArrayList<Integer>();
        for(int i=1;i<201;i++){
            list.add(i);
        }
        levelAdapter=new LevelAdapter(list, this);
        recyclerView.setLayoutManager(new GridLayoutManager(this,5));
        recyclerView.setAdapter(levelAdapter);
    }
    void initializeView(){
        toolbarLevel=(TextView)findViewById(R.id.level);
        refreshButton=(ImageButton)findViewById(R.id.refresh);
        toolbarLevel.setText(gameLevelName);
        refreshButton.setOnClickListener(null);
        backButton=(ImageButton)findViewById(R.id.back);
        backButton.setOnClickListener(levelSelectorActivityListener);

    }

    void getIntentData(){
        Intent intent=getIntent();
        gameLevelName=intent.getStringExtra(getString(R.string.gameLevelName));
        //gameLevelScore=intent.getIntExtra(getString(R.string.gameLevelScore),0);
    }

    void initializeStringResource(){
        levelPreSchool=getString(R.string.levelPreSchool);
        levelPrimary=getString(R.string.levelPrimary);
        levelJunior=getString(R.string.levelJunior);
        levelSecondary=getString(R.string.levelSecondary);
        levelAdvance=getString(R.string.levelAdvance);
        modelPreSchool=getString(R.string.modelPreSchool);
        modelPrimary=getString(R.string.modelPrimary);
        modelJunior=getString(R.string.modelJunior);
        modelSecondary=getString(R.string.modelSecondary);
        modelAdvance=getString(R.string.modelAdvance);
        solutionPreSchool=getString(R.string.solutionPreSchool);
        solutionPrimary=getString(R.string.solutionPrimary);
        solutionJunior=getString(R.string.solutionJunior);
        solutionSecondary=getString(R.string.solutionSecondary);
        solutionAdvance=getString(R.string.solutionAdvance);
        modelFileName=new HashMap<>();
        solutionFileName=new HashMap<>();
        modelFileName.put(levelPreSchool,modelPreSchool);
        modelFileName.put(levelPrimary,modelPrimary);
        modelFileName.put(levelJunior,modelJunior);
        modelFileName.put(levelSecondary,modelSecondary);
        modelFileName.put(levelAdvance,modelAdvance);
        solutionFileName.put(levelPreSchool,solutionPreSchool);
        solutionFileName.put(levelJunior,solutionJunior);
        solutionFileName.put(levelPrimary,solutionPrimary);
        solutionFileName.put(levelSecondary,solutionSecondary);
        solutionFileName.put(levelAdvance,solutionAdvance);

    }
    void storeSolutionData(){
        AssetManager am = getApplicationContext().getAssets();
        InputStream is=null;
        try {
             is = am.open(solutionFileName.get(gameLevelName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<String[]> list=Utility.readFile(is);
        Utility.solution.put(gameLevelName,list);
    }
    void storeModelData(){
        AssetManager am = getApplicationContext().getAssets();
        InputStream is=null;
        try {
            is = am.open(modelFileName.get(gameLevelName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<String[]> list=Utility.readFile(is);
        Utility.model.put(gameLevelName,list);

    }

    View.OnClickListener levelSelectorActivityListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch(view.getId()){
                case R.id.back:
                    onBackPressed();
                    break;
                case  R.id.refresh:
            }
        }
    };

}
