package com.middaydreamz.shobdokolpo;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;

public class IntroActivity extends AppCompatActivity {

    View itemOne, itemTwo, itemThree, itemFour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_intro);


        itemOne = findViewById(R.id.itemOne);
        itemTwo = findViewById(R.id.itemTwo);
        itemThree = findViewById(R.id.itemThree);
        itemFour = findViewById(R.id.itemFour);

        Handler h = new Handler(Looper.getMainLooper());

        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                animate(null);
            }
        }, 500);
    }

    /*private void moveViewToScreenCenter(View view) {
        LinearLayout root = (LinearLayout) findViewById(R.id.rootLayout);
        DisplayMetrics dm = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int statusBarOffset = dm.heightPixels - root.getMeasuredHeight();

        int originalPos[] = new int[2];
        view.getLocationOnScreen(originalPos);

        int xDest = dm.widthPixels / 2;
        xDest -= (view.getMeasuredWidth() / 2);
        int yDest = dm.heightPixels / 2 - (view.getMeasuredHeight() / 2) - statusBarOffset;

        TranslateAnimation anim = new TranslateAnimation(xDest - originalPos[0], 0, yDest - originalPos[1], 0);
        anim.setDuration(800);
        anim.setFillAfter(true);
        view.startAnimation(anim);
    }*/


    void moveViewToFinalLoc(View view, int position) {
        LinearLayout root = (LinearLayout) findViewById(R.id.rootLayout);

        DisplayMetrics dm = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(dm);
        //int statusBarOffset = dm.heightPixels - root.getMeasuredHeight();

        int originalPos[] = new int[2];
        view.getLocationOnScreen(originalPos);

        int abspos[] = new int[2];

        View abs = itemOne;

        if (position == 0) {
            abs = itemOne;
        } else if (position == 1) {
            abs = itemTwo;
        } else if (position == 2) {
            abs = itemThree;
        } else if (position == 3) {
            abs = itemFour;
        }

        abs.getLocationOnScreen(abspos);
        int xDest = abspos[0];
        //xDest -= (view.getMeasuredWidth() / 2);

        int yDest = abspos[1];

        TranslateAnimation anim = new TranslateAnimation(0, xDest - originalPos[0], 0, yDest - originalPos[1]);
        anim.setDuration(800);
        anim.setFillAfter(true);
        view.startAnimation(anim);

        if (position == 3) {

            animateOrg();
            //end();
        }
    }

    public void animateOrg() {
        Handler h1 = new Handler();
        h1.postDelayed(new Runnable() {
            @Override
            public void run() {
                String s = "by<br/><br/><b>MiddayDreamz</b>";
                Spanned s1;

                if (Build.VERSION.SDK_INT >= 24) {
                    s1 = Html.fromHtml(s, Html.FROM_HTML_MODE_LEGACY);
                } else {
                    s1 = Html.fromHtml(s);
                }

                TextView orgName = (TextView) findViewById(R.id.org);
                orgName.setVisibility(View.VISIBLE);
                orgName.setText(s1);

                //tv2.setVisibility(View.VISIBLE);
                AlphaAnimation fadeIn = new AlphaAnimation(0.0f, 1.0f);
                //protected AlphaAnimation fadeOut = new AlphaAnimation( 1.0f , 0.0f ) ;
                orgName.startAnimation(fadeIn);
                //txtView.startAnimation(fadeOut);
                fadeIn.setDuration(1000);
                fadeIn.setFillAfter(true);
                //fadeOut.setDuration(1200);
                //fadeOut.setFillAfter(true);
                //fadeOut.setStartOffset(4200+fadeIn.getStartOffset());

                end();
            }
        }, 100);
    }

    public void animate(View view) {
       /* itemOne.setAnimation(animation);
        itemTwo.setAnimation(animation);
        itemThree.setAnimation(animation);
        itemFour.setAnimation(animation);*/

        /*itemOne.startAnimation(animation);
        itemTwo.startAnimation(animation);
        itemThree.startAnimation(animation);
        itemFour.startAnimation(animation);*/

        /*moveViewToScreenCenter(itemOne);
        moveViewToScreenCenter(itemTwo);
        moveViewToScreenCenter(itemThree);
        moveViewToScreenCenter(itemFour);*/

        moveViewToFinalLoc(findViewById(R.id.itemOneabs), 0);
        moveViewToFinalLoc(findViewById(R.id.itemTwoabs), 1);
        moveViewToFinalLoc(findViewById(R.id.itemThreeabs), 2);
        moveViewToFinalLoc(findViewById(R.id.itemFourabs), 3);


        //end();
    }

    void end() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(IntroActivity.this, HomeActivity.class));
                overridePendingTransition(R.anim.slide_enter_top_bottom, R.anim.slide_exit_top_bottom);
                finish();
            }
        }, 2000);
    }
}
