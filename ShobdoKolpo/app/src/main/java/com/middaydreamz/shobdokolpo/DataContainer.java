package com.middaydreamz.shobdokolpo;

import android.graphics.drawable.Drawable;

/**
 * Created by USER on 5/25/2017.
 */

public class DataContainer {
    private String puzzleData;
    private String solutionData;
    private int flag;
    private int colorContent;
    //private int colorBackGround;
    private Drawable drawableBackground;

    public String getPuzzleData() {
        return puzzleData;
    }

    public String getSolutionData() {
        return solutionData;
    }

    public int getFlag() {
        return flag;
    }

    public int getColorContent() {
        return colorContent;
    }

    public void setColorContent(int colorContent) {
        this.colorContent = colorContent;
    }

   /* public int getColorBackGround() {
        return colorBackGround;
    }

    public void setColorBackGround(int colorBackGround) {
        this.colorBackGround = colorBackGround;
    }*/

    public Drawable getDrawableBackground() {
        return drawableBackground;
    }

    public void setDrawableBackground(Drawable drawableBackground) {
        this.drawableBackground = drawableBackground;
    }

    public DataContainer(String puzzleData, String solutionData, int flag, Drawable drawableBackground, int colorContent) {

        this.puzzleData = puzzleData;
        this.solutionData = solutionData;
        this.flag = flag;
        this.drawableBackground=drawableBackground;
        //this.colorBackGround=colorBackGround;
        this.colorContent=colorContent;

    }

    public void setPuzzleData(String puzzleData) {
        this.puzzleData = puzzleData;
    }

    public void setSolutionData(String solutionData) {
        this.solutionData = solutionData;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }
}
