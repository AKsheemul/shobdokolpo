package com.middaydreamz.shobdokolpo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class LevelPacksActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_packs);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(this, "Pause",Toast.LENGTH_LONG).show();
        onDestroy();
    }
}
